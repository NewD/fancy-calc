import { createMuiTheme } from '@material-ui/core'

export const dayTheme = createMuiTheme({
	palette: {
		primary: {
			main: '#005d76',
		},
		secondary: {
			main: '#0293d1',
		},
		background: {
			main: '#c8c8c8',
			paper: '#fafbfc',
		},
	},
	input: {
		color: 'black',
	},
	inputLabel: {
		color: 'grey',
	},
})

export const nightTheme = createMuiTheme({
	palette: {
		primary: {
			main: '#005d76',
		},
		secondary: {
			main: '#edeef0',
		},
		default: {
			main: '#edeef0',
		},
		background: {
			main: '#292f44',
			paper: '#4b4261',
		},
	},
	typography: {
		h5: {
			color: '#edeef0',
		},
	},
	input: {
		color: 'white',
	},
	inputLabel: {
		color: 'lightgrey',
	},
})
