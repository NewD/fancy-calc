import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import { App } from './core/App'
import ThemeProvider from './providers/ThemeProvider'
import { rootReducer } from './redux/reducers/root'
import './style.css'
import 'core-js/modules/es6.promise'
import 'core-js/modules/es6.array.iterator'

const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(thunk),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
)

const app = (
	<Provider store={store}>
		<ThemeProvider children={<App />} />
	</Provider>
)

ReactDOM.render(app, document.getElementById('root'))
