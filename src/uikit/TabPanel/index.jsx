import { CircularProgress } from '@material-ui/core'
import React, { Suspense } from 'react'

function TabPanel({ value, index, children }) {
	
	return (
		<div>
			{/* <Suspense fallback={loader}> */}
				{value === index && React.createElement(children)}
			{/* </Suspense> */}
		</div>
	)
}

export default TabPanel
