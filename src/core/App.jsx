import { Box, Container, useTheme } from '@material-ui/core'
import React from 'react'
import Content from '../components/Content'
import Header from '../components/Header'

import * as modules from 'src/modules'

function App() {
	const activeModulesNames = ['nod_calc', 'triangle_square_calc', 'preview']
	const activeModules = Object.values(modules).filter(module =>
		activeModulesNames.includes(module.name)
	)

	const headerData = activeModules.map(({ name, title }) => ({ name, title }))
	const contentData = activeModules.map(({ name, component }) => ({
		name,
		component,
	}))

	return (
		<Box
			bgcolor="background.main"
			style={{ height: '100vh' }}
			component="section">
			<Header modules={headerData} />
			<Content modules={contentData} />
		</Box>
	)
}

export { App }
