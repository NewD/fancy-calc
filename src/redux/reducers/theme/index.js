import { CHANGE_THEME } from 'src/redux/types'

const initialState = { dayTime: sessionStorage.getItem('isLight') || 'day' }

export function themeReducer(state = initialState, action) {
	switch (action.type) {
		case CHANGE_THEME:
			return { ...state, dayTime: action.payload }

		default:
			return state
	}
}
