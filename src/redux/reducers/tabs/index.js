import { CHANGE_TAB } from 'src/redux/types'

const initialState = {
	currentTab: +sessionStorage.getItem('currentTab') || 0,
}

export function tabsReducer(state = initialState, action) {
	switch (action.type) {
		case CHANGE_TAB:
			return { ...state, currentTab: action.payload }

		default:
			return state
	}
}
