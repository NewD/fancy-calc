import { combineReducers } from 'redux'
import { tabsReducer } from './tabs'
import { themeReducer } from './theme'

export const rootReducer = combineReducers({
	tabs: tabsReducer,
	theme: themeReducer,
})
