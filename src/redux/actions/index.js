import { CHANGE_TAB, CHANGE_THEME } from '../types'

export function changeTab(event, tabNumber) {
	sessionStorage.setItem('currentTab', tabNumber)
	return { type: CHANGE_TAB, payload: tabNumber }
}

export function changeTheme(dayTime) {
	sessionStorage.setItem('isLight', dayTime)
	return { type: CHANGE_THEME, payload: dayTime }
}
