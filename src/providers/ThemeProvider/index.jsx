import React from 'react'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core'
import { dayTheme, nightTheme } from 'src/themes'
import { useSelector } from 'react-redux'

function ThemeProvider({ children }) {
	const dayTime = useSelector(state => state.theme.dayTime)

	const currentTheme = dayTime === 'day' ? dayTheme : nightTheme

	return <MuiThemeProvider theme={currentTheme}>{children}</MuiThemeProvider>
}

export default ThemeProvider
