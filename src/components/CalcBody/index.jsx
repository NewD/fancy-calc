import React, { Suspense } from 'react'
import { CircularProgress, Paper } from '@material-ui/core'

import { styles } from './style'

function CalcBody({ calculators }) {
	const classes = styles()
	
	const loader = (
		<div
			style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
			}}>
			<CircularProgress />
		</div>
	)

	return (
		<div>
			<Paper className={classes.card}>
				<div className={classes.display}>
					<Suspense fallback={loader}>{calculators}</Suspense>
				</div>
			</Paper>
		</div>
	)
}

export default CalcBody
