import { makeStyles } from '@material-ui/core'

export const styles = makeStyles({
	card: {
		marginTop: '40px',

		padding: '16px',
		maxWidth: '300px',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	display: {
		width: '100%',
	},
})
