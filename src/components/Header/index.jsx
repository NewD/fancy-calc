import React, { useCallback } from 'react'
import { Container, AppBar, Tab, Tabs } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { changeTab } from 'src/redux/actions'
import ThemeChanger from '../ThemeChanger'

function Header({ modules }) {
	const tabs = modules.map(module => (
		<Tab label={module.title} key={module.name} />
	))

	const dispatch = useDispatch()

	const currentTab = useSelector(state => state.tabs.currentTab)

	const shiftTab = useCallback(
		(e, tabNumber) => dispatch(changeTab(e, tabNumber)),
		[]
	)

	return (
		<AppBar position="static" component="header">
			<Container
				maxWidth="md"
				style={{ display: 'flex', alignItems: 'center' }}>
				<Tabs
					style={{ marginRight: '50px' }}
					indicatorColor={'secondary'}
					value={currentTab}
					onChange={shiftTab}>
					{tabs}
				</Tabs>
				<ThemeChanger />
			</Container>
		</AppBar>
	)
}

export default Header
