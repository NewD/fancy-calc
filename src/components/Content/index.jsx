import React, { Suspense } from 'react'
import { CircularProgress, Container } from '@material-ui/core'
import TabPanel from 'src/uikit/TabPanel'
import { useSelector } from 'react-redux'
import CalcBody from 'src/components/CalcBody'

function Content({ modules }) {
	const currentTab = useSelector(state => state.tabs.currentTab)

	const calculators = modules.map((module, index) => (
		<TabPanel
			value={currentTab}
			index={index}
			children={module.component}
			key={module.name}
		/>
	))

	return (
		<Container maxWidth="md">
			<CalcBody calculators={calculators} />
		</Container>
	)
}

export default Content
