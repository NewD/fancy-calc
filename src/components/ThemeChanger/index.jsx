import { Brightness3, WbSunny } from '@material-ui/icons'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { changeTheme } from 'src/redux/actions'

function ThemeChanger() {
	const currentTheme = useSelector(state => state.theme.dayTime)

	const dispatch = useDispatch()

	const selectTheme = currentTheme => {
		const actualTheme = currentTheme === 'day' ? 'night' : 'day'
		dispatch(changeTheme(actualTheme))
	}

	const icon = currentTheme === 'day' ? <Brightness3 style={{transform: 'rotate(-216deg)'}} /> : <WbSunny />

	return <div onClick={() => selectTheme(currentTheme)}>{icon}</div>
}

export default ThemeChanger
