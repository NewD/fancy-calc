import React from 'react'
import NODCalc from './moduleRoot'

export const NODCalcModule = {
	name: 'nod_calc',
	component: React.lazy(() => {
		return Promise.all([
			import('./moduleRoot'),
			new Promise(resolve => setTimeout(resolve, 1000)),
		]).then(([moduleExports]) => moduleExports)
	}),
	title: 'Greatest common divisor',
}
