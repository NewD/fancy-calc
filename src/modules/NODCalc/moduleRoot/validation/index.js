import * as yup from 'yup'

export const schema = yup.object().shape({
	first: yup.number().positive().integer(),
	second: yup.number().positive().integer(),
})
