import React, { useState } from 'react'
import { Button, TextField, Typography, useTheme } from '@material-ui/core'
import { calcNOD } from './calcNOD'
import { styles } from './style'
import { useForm } from 'react-hook-form'
import { schema } from './validation'
import { yupResolver } from '@hookform/resolvers'
import { textFieldNames } from './itemsConfig'

function NODCalc() {
	const classes = styles()
	const [result, getResult] = useState('')

	const { register, handleSubmit, errors } = useForm({
		resolver: yupResolver(schema),
	})
	const themeInput = useTheme().input
	const themeInputLabel = useTheme().inputLabel

	const textFields = textFieldNames.map(name => (
		<TextField
			inputRef={register}
			name={name}
			className={classes.input}
			color="secondary"
			InputProps={{ style: themeInput, className: classes.input }}
			label={name}
			InputLabelProps={{ style: themeInputLabel }}
			error={errors[name] ? true : false}
			helperText={errors[name] ? 'Enter number!' : ' '}
			key={name}
		/>
	))

	return (
		<form
			className={classes.nodCalcWrapper}
			onSubmit={handleSubmit(data =>
				getResult(calcNOD(data.first, data.second))
			)}>
			<Typography className={classes.title} align="center" variant="h5">
				Greatest common divisor
			</Typography>

			<div className={classes.inputDisplay}>{textFields}</div>

			<Typography
				color="secondary"
				className={classes.result}
				children={`Result: ${result}`}
			/>
			<Button type="submit" variant="contained" color="primary">
				Сalculate!
			</Button>
		</form>
	)
}

export default NODCalc