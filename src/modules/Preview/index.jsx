import React from 'react'

export const PreviewModule = {
	name: 'preview',
	component: React.lazy(() => import('./moduleRoot')),
	title: 'home',
}
