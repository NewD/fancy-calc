import { Typography } from '@material-ui/core'
import React from 'react'

function Preview() {
	return (
		<Typography align="center" variant="h5">
			Welcome to Fancy calc!
		</Typography>
	)
}

export default Preview
