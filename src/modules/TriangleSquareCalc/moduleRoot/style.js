import { makeStyles } from '@material-ui/core'

export const styles = makeStyles({
	title: {
		marginBottom: '20px',
	},
	nodCalcWrapper: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		width: '100%',
		marginBottom: '20px',
	},
	input: {
		width: '80px',
	},
	inputDisplay: {
		display: 'flex',
		justifyContent: 'space-evenly',
		marginBottom: '20px',
	},
	result: {
		marginLeft: '20px',
		marginBottom: '20px',
	},
})
