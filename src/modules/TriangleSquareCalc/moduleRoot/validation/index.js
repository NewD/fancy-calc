import * as yup from 'yup'

export const schema = yup.object().shape({
	first: yup.number().positive(),
	second: yup.number().positive(),
	third: yup.number().positive(),
})
