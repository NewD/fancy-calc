import React from 'react'
import { TextField, useTheme } from '@material-ui/core'

function TextFields({ names, inputClasses, errors, register }) {
	const themeInput = useTheme().input
	const themeInputLabel = useTheme().inputLabel

	const textFields = names.map(name => (
		<TextField
			inputRef={register}
			name={name}
			className={inputClasses}
			color="secondary"
			InputProps={{ style: themeInput, className: inputClasses }}
			label={name}
			InputLabelProps={{ style: themeInputLabel }}
			error={errors[name] ? true : false}
			helperText={errors[name] ? 'Enter number!' : ' '}
			key={name}
		/>
	))
	return <>{textFields}</>
}

export default TextFields
