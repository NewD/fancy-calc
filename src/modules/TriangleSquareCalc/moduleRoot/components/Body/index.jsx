import React, { useState } from 'react'
import { Button, Typography } from '@material-ui/core'
import TextFields from '../TextFields'
import { textFieldNames } from '../../itemsConfig'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers'
import { schema } from '../../validation'
import { triangleSquare } from '../../triangleSquare'

function Body({ classes }) {
	const [result, getResult] = useState('')
	const fieldNames = textFieldNames

	const { register, handleSubmit, errors } = useForm({
		resolver: yupResolver(schema),
	})

	return (
		<form
			className={classes.nodCalcWrapper}
			onSubmit={handleSubmit(data =>
				getResult(triangleSquare(data.first, data.second, data.third))
			)}>
			<Typography className={classes.title} align="center" variant="h5">
				Triangle square
			</Typography>

			<div className={classes.inputDisplay}>
				<TextFields
					names={fieldNames}
					inputClasses={classes.input}
					errors={errors}
					register={register}
				/>
			</div>

			<Typography
				color="secondary"
				className={classes.result}
				children={`Result: ${result}`}
			/>

			<Button type="submit" variant="contained" color="primary">
				Сalculate!
			</Button>
		</form>
	)
}

export default Body
