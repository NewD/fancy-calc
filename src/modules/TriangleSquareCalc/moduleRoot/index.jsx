import React from 'react'
import Body from './components/Body'
import { styles } from './style'

function TriangleSquareCalc() {
	const classes = styles()
	return (
		<>
			<Body classes={classes} />
		</>
	)
}

export default TriangleSquareCalc
