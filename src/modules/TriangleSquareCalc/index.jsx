import React from 'react'

export default {
	name: 'triangle_square_calc',
	component: React.lazy(() => {
		return Promise.all([
			import('./moduleRoot'),
			new Promise(resolve => setTimeout(resolve, 2000)),
		]).then(([moduleExports]) => moduleExports)
	}),
	title: 'triangle square calc',
}
